var spark = require('ciscospark/env');
var express = require('express');
var app = express();
var path = require('path');
var bodyParser = require('body-parser');
var request = require('request');
var myParser = bodyParser.urlencoded({extended:false});

var answer = '';
var question = '';
var ciscoMsg = '';
var responseGood = '';
var responseBad = '';
var solved = false;

app.use(express.static('./public'));
app.use(bodyParser.json());
app.set('view engine','ejs');

app.get('*', function(req,res){
  res.render('index');
});


app.post('/action', myParser, function(req,res){
  question = req.body.question;
  var textToCisco = "A new question has been just created! \n" + question;
  answer = req.body.answer;
  spark.messages.create({
    text: question,
    roomId:'Y2lzY29zcGFyazovL3VzL1JPT00vMTQ2NTBkMDAtN2I1YS0xMWU4LTgzNjYtN2RmNzBhOGZkY2Yy'
  });
  res.render('after');
});

app.post('/', function(req,res){
  if(!solved){
  var text = '';
  var msg = req.body.data.id;
  var urlMessage = 'https://api.ciscospark.com/v1/messages/' + msg;
  var auth = 'Bearer ZDI5YWQyMDMtY2QxOS00ZGU0LWJlMTUtOTNjMDY0NzMzODgzY2Q4ZmNiMzUtZmVk';
  var personId = req.body.data.personId;
  var urlPerson = 'https://api.ciscospark.com/v1/people/' + personId;

  request({
        url : urlMessage,
        headers : {
            "Authorization" : auth
        }
    },
    function (error, res, body) {
      function jsonParser(stringValue) {
        var objectValue = JSON.parse(stringValue);
       return objectValue['text'];
      };

    text = jsonParser(body);

    if(text.toUpperCase() === answer.toUpperCase()){
      request({
            url : urlPerson,
            headers : {
                "Authorization" : auth
            }
        },
        function (error, res, body) {
          function jsonParser(stringValue) {
            var objectValue = JSON.parse(stringValue);
           return objectValue['nickName'];
          };
        name = jsonParser(body);
        responseGood = name + " Got it right!, the answer was " + answer;
        spark.messages.create({
          text: responseGood,
          roomId:'Y2lzY29zcGFyazovL3VzL1JPT00vMTQ2NTBkMDAtN2I1YS0xMWU4LTgzNjYtN2RmNzBhOGZkY2Yy'
        });
        solved = true;
        });
    }
    else if(text != question && text != responseBad){
      responseBad = "Incorrect! Keep Trying!";
      spark.messages.create({
        text: responseBad,
        roomId:'Y2lzY29zcGFyazovL3VzL1JPT00vMTQ2NTBkMDAtN2I1YS0xMWU4LTgzNjYtN2RmNzBhOGZkY2Yy'
      });
    }
    });
  }
});

app.listen(5000);

console.log("Listening on port 5000");
